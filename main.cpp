#ifndef __PROGTEST__
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <vector>
using namespace std;

using State = unsigned int;
using Symbol = char;

struct MISNFA {
    std::set < State > m_States;
    std::set < Symbol > m_Alphabet;
    std::map < std::pair < State, Symbol >, std::set < State > > m_Transitions;
    std::set < State > m_InitialStates;
    std::set < State > m_FinalStates;
};

struct DFA {
    std::set < State > m_States;
    std::set < Symbol > m_Alphabet;
    std::map < std::pair < State, Symbol >, State > m_Transitions;
    State m_InitialState;
    std::set < State > m_FinalStates;

    bool operator== ( const DFA & other ) {
        return
                std::tie ( m_States, m_Alphabet, m_Transitions, m_InitialState, m_FinalStates ) ==
                std::tie ( other.m_States, other.m_Alphabet, other.m_Transitions, other.m_InitialState, other.m_FinalStates );
    }
};
#endif

map<int,set<State>>::iterator findInMap( map<int , set<State>> & newStates, const set<State>& target ){
    for (map<int,set<State>>::iterator it = newStates.begin() ; it != newStates.end() ; it++ ){
        if (it->second == target)
            return it;
    }
    return newStates.end();
}

DFA determinize ( const MISNFA & nfa ) {
    DFA toReturn;
    toReturn.m_InitialState = 0;
    State current = 1;
    int index = 0;
    toReturn.m_Alphabet =nfa.m_Alphabet;
    State TrashState;
    bool EmptyCreated = false;
    map<int , set<State>> newStates;
    /*Merges initials states*/
    for (auto it : nfa.m_InitialStates){
        newStates[0].insert(it);
    }
    toReturn.m_States.insert(0);
    /*Creates states*/
    while( (size_t)index != newStates.size() ) {
        for (auto aph : nfa.m_Alphabet) {
            set<State> toInsert;
            for (auto it : newStates[index]) {
                auto find = nfa.m_Transitions.find(make_pair(it, aph));
                if (find != nfa.m_Transitions.end()) {
                    for (auto merge : find->second) {
                        toInsert.insert(merge);
                    }
                }
            }
            if (!toInsert.empty()) {
                if (findInMap(newStates, toInsert) != newStates.end()) {
                    toReturn.m_Transitions.insert(make_pair(make_pair(index, aph), findInMap(newStates, toInsert)->first));
                } else {
                    toReturn.m_Transitions.insert(std::make_pair((std::make_pair(index, aph)), current));
                    newStates.insert(std::make_pair(current, toInsert));
                    toReturn.m_States.insert(current++);
                }
            }
            else {
                if (!EmptyCreated){
                    TrashState = current;
                    toReturn.m_States.insert(current);
                    for (auto itL : nfa.m_Alphabet){
                        toReturn.m_Transitions.insert(
                                make_pair(make_pair(TrashState,itL), TrashState)
                        );
                    }
                    toReturn.m_Transitions.insert(make_pair(make_pair(index,aph),TrashState));
                    current++;
                    EmptyCreated = true;
                }
                else
                    toReturn.m_Transitions.insert(make_pair(make_pair(index,aph),TrashState));
            }
        }
        index++;
    }
    /*Creates final*/
    for (auto it : nfa.m_FinalStates){
        for (auto it2 : newStates){
            if (it2.second.find(it) != it2.second.end()){
                toReturn.m_FinalStates.insert(it2.first);
            }
        }
    }
    return toReturn;
}

int countConnectionsToItself(DFA &dfa, unsigned int state) {
    int count = 0;
    for (auto alpha : dfa.m_Alphabet ){
        if ( dfa.m_Transitions.find(make_pair(state,alpha)) != dfa.m_Transitions.end() && dfa.m_Transitions.find(make_pair(state,alpha))->second == state){
            count++;
        }
    }
    return count;
}

void eraseOtherTrans(DFA & res, std::set < State > states){
    std::set <std::pair < State, Symbol > > toErase;
    for(auto &it : res.m_Transitions){
        if(states.find(it.second) != states.end()){ //pokud ten stav tam je tak musime tuto transakci smazat
            toErase.insert(it.first);
        }
    }
    std::set<State> tmp2;
    for(auto &it : toErase){
        State tmp = it.first;
        res.m_Transitions.erase(it);
        if(countConnectionsToItself(res , tmp)!= 0 &&
           res.m_FinalStates.find(tmp) == res.m_FinalStates.end() &&
           res.m_InitialState != tmp) {
            res.m_States.erase(tmp);
            for(auto it2 : res.m_Alphabet)
                res.m_Transitions.erase( make_pair(tmp, it2) );

            tmp2.insert(tmp);
        }
    }
    if(!tmp2.empty())
        eraseOtherTrans(res, tmp2);
}

bool IsHeadingToEndPoint( DFA & dfa , set<State> result , State number ){
    set<State> tmp;
    vector<State> stack;
    stack.push_back(number);

    while(true){
        if (stack.empty())
            return false;
        State current = *stack.begin();
        stack.erase(stack.begin());
        for (auto alpha : dfa.m_Alphabet){
            map < std::pair < State, Symbol >, State >::iterator it = dfa.m_Transitions.find(make_pair(current, alpha));
            if ( it != dfa.m_Transitions.end()){
                State toCheck = it->second;
                if (dfa.m_FinalStates.find(toCheck) != dfa.m_FinalStates.end()){
                    result.insert(toCheck);
                    return true;
                }
                else if (result.find(toCheck) != result.end())
                    return true;
                else if (tmp.find(toCheck) == tmp.end() || current != toCheck){
                    tmp.insert(toCheck);
                    stack.push_back(toCheck);
                }
            }
        }
    }
}

DFA RenameDFA(DFA &dfa) {
    DFA toReturn;
    int index = 0;
    map<int , int> newStates;
    for (auto it : dfa.m_States)
        newStates.insert(make_pair(it,index++));
    for (int i = 0 ; i < index ; i++)
        toReturn.m_States.insert(i);
    for (auto it : dfa.m_FinalStates)
        toReturn.m_FinalStates.insert(newStates.find(it)->second);

    toReturn.m_InitialState = newStates.find(dfa.m_InitialState)->second;
    for(auto it : dfa.m_Transitions)
        toReturn.m_Transitions.insert(make_pair(make_pair(newStates.find(it.first.first)->second, it.first.second), newStates.find(it.second)->second));
    toReturn.m_Alphabet = dfa.m_Alphabet;
    return toReturn;
}

bool isFinalState(unsigned int state, DFA dfa) {
    if (dfa.m_FinalStates.find(state) != dfa.m_FinalStates.end())
        return true;
    return false;
}

void delete2(DFA & dfa, set<State> AllStates) {
    set < pair<State, Symbol> > toDelete;
    for (auto iter : dfa.m_Transitions){
        if (AllStates.find(iter.second) != AllStates.end())
            toDelete.insert(iter.first);
    }
    set<unsigned int> recStack;
    for (auto toDel : toDelete){
        State current = toDel.first;
        dfa.m_Transitions.erase(toDel);
        if (dfa.m_FinalStates.find(current) == dfa.m_FinalStates.end() &&
            dfa.m_InitialState != current &&
            countConnectionsToItself(dfa , current) != 0){
            dfa.m_States.erase(current);
            for (auto alpha : dfa.m_Alphabet)
                dfa.m_Transitions.erase(make_pair(current,alpha));

            recStack.insert(current);
        }
        if (recStack.size() != 0)
            delete2(dfa , recStack);
    }
}

DFA trim (const DFA & dfa ) {
    DFA toReturn = dfa;

    set<unsigned int> tmp = toReturn.m_States;
    set<unsigned int> found;
    tmp.erase(toReturn.m_InitialState);

    for( auto item : toReturn.m_Transitions)
        found.insert(item.second);

    set<State> toDel;
    for (auto it : tmp){
        if (found.find(it) == found.end())
            toDel.insert(it);
    }

    for (auto it : toDel){
        if (it != toReturn.m_InitialState)
        toReturn.m_States.erase(it);
        for(auto it2 : toReturn.m_Alphabet)
            toReturn.m_Transitions.erase( make_pair(it, it2));
    }

    //ITERATION
    set<State> stack;
    set<State> result;

    for (auto state : toReturn.m_States){
        if (!isFinalState(state, toReturn)){
            if ( !IsHeadingToEndPoint(toReturn , stack, state) )
                result.insert(state);
            else
                stack.insert(state);
        }
    }
    for (auto it : result){
        toReturn.m_States.erase(it);
        for(auto it2 : toReturn.m_Alphabet)
            toReturn.m_Transitions.erase( make_pair(it, it2));
    }

    delete2(toReturn, result);

    DFA toReturn2 = RenameDFA(toReturn);
    return toReturn2;
}

DFA minimize ( const DFA & dfa ) {
    DFA toReturn = dfa;

    return toReturn;
}










#ifndef __PROGTEST__

#include "sample.h"

int main ( ) {
    // trim
//    assert ( trim ( determinize ( in6 ) ) == outT6 );

    assert ( trim ( determinize ( in13 ) ) == outT13 );
    assert ( trim ( determinize ( in2 ) ) == outT2 );
    assert ( trim ( determinize ( in1 ) ) == outT1 );
    assert ( trim ( determinize ( in0 ) ) == outT0 );
    assert ( trim ( determinize ( in3 ) ) == outT3 );
    assert ( trim ( determinize ( in4 ) ) == outT4 );
    assert ( trim ( determinize ( in5 ) ) == outT5 );
    assert ( trim ( determinize ( in9 ) ) == outT9 );
    assert ( trim ( determinize ( in11 ) ) == outT11 );
    assert ( trim ( determinize ( in12 ) ) == outT12 );
    assert ( trim ( determinize ( in7 ) ) == outT7 );
    assert ( trim ( determinize ( in8 ) ) == outT8 );
    assert ( trim ( determinize ( in10 ) ) == outT10 );


    // minimize
    assert ( minimize ( trim ( determinize ( in0 ) ) ) == outM0 );
    assert ( minimize ( trim ( determinize ( in1 ) ) ) == outM1 );
    assert ( minimize ( trim ( determinize ( in2 ) ) ) == outM2 );
    assert ( minimize ( trim ( determinize ( in3 ) ) ) == outM3 );
    assert ( minimize ( trim ( determinize ( in4 ) ) ) == outM4 );
    assert ( minimize ( trim ( determinize ( in5 ) ) ) == outM5 );
    assert ( minimize ( trim ( determinize ( in6 ) ) ) == outM6 );
    assert ( minimize ( trim ( determinize ( in7 ) ) ) == outM7 );
    assert ( minimize ( trim ( determinize ( in8 ) ) ) == outM8 );
    assert ( minimize ( trim ( determinize ( in9 ) ) ) == outM9 );
    assert ( minimize ( trim ( determinize ( in10 ) ) ) == outM10 );
    assert ( minimize ( trim ( determinize ( in11 ) ) ) == outM11 );
    assert ( minimize ( trim ( determinize ( in12 ) ) ) == outM12 );
    assert ( minimize ( trim ( determinize ( in13 ) ) ) == outM13 );

    return 0;
}
#endif